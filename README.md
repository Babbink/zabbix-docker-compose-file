# Setup a Zabbix Server within minutes

Requirements
1. Docker is installed
2. Docker Compose is installed

Download the file and run ` docker-compose up -d`. After about 2 minutes the Zabbix Server is ready and you can login with Admin:zabbix on http://localhost/.
